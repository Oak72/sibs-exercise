package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.controllers;

import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.StockMovement;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.User;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.services.StockMovementService;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("stock")
public class StockMovementController {

    @Autowired
    StockMovementService stockMovementService;

    @GetMapping("/list")
    private List<StockMovement> getAllStockMovements() {
        return stockMovementService.getAllStockMovements();
    }

    @GetMapping({"/create/{itemType}/{quantity}"})
    private StockMovement createStockMovement(
            @PathVariable String itemType,
            @PathVariable String quantity
    ) {

        return stockMovementService.save(itemType, quantity);

    }

    @GetMapping({"/update/{stockMovementId}/{itemType}/{quantity}"})
    private StockMovement createOrUpdateStockMovement(
            @PathVariable String stockMovementId,
            @PathVariable String itemType,
            @PathVariable String quantity) {

        return stockMovementService.update(stockMovementId, itemType, quantity);

    }

    @GetMapping("/read/{id}")
    private StockMovement getStockMovement(@PathVariable("id") int id) {
        return stockMovementService.getStockMovementsById(id);
    }

    @GetMapping("/delete/{id}")
    private void deleteStockMovement(@PathVariable("id") int id) {
        stockMovementService.delete(id);
    }

}
