package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.services;

import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.Order;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.StockMovement;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.repository.ItemsRepository;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.repository.StockMovementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StockMovementService {


    @Autowired
    StockMovementRepository stockMovementRepository;
    @Autowired
    ItemService itemService;
    public List<StockMovement> getAllStockMovements() {
        List<StockMovement> movements = new ArrayList<>();
        stockMovementRepository.findAll().forEach(movements::add);
        return movements;
    }


    public StockMovement save(String itemType, String quantity) {
        return stockMovementRepository.save(
                StockMovement.from(
                        itemService.getByName(itemType),
                        Integer.parseInt(quantity),
                        System.currentTimeMillis()
                ));
    }

    public StockMovement update(String stockMovementId, String itemType, String quantity) {
        StockMovement stockMovement = stockMovementRepository.findById(Integer.valueOf(stockMovementId)).get();
        stockMovement.setItem(itemService.getByName(itemType));
        stockMovement.setQuantity(Integer.parseInt(quantity));
        return stockMovementRepository.save(stockMovement);

    }

    public StockMovement getStockMovementsById(int id) {
        return stockMovementRepository.findById(id).get();
    }

    public void delete(int id) {
        stockMovementRepository.deleteById(id);
    }

}
