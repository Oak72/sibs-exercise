package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpStatusCodeException;

public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public HttpStatus handleException() {

        return HttpStatus.BAD_REQUEST;
    }
}
