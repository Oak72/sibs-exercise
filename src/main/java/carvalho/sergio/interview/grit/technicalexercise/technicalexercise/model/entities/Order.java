package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities;

import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.Item;

import javax.persistence.*;

public class Order {

    @Id
    @Column
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    @Column
    private final long creationDate;

    @MapsId
    @OneToMany
    @JoinColumn(name = "item_id")
    private Item item;
    @Column
    private int quantity;

    @MapsId
    @OneToMany
    @JoinColumn(name = "user_id")
    private User user;

    private Order (Item item, int quantity, User user, long creationDate) {
        this.user = user;
        this.item = item;
        this.quantity = quantity;
        this.creationDate = creationDate;

    }

    public static Order from(Item item, int quantity, User user, long creationDate) {
        return new Order(item, quantity, user, creationDate);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    /*

This is a simple exercise, a simple order manager.
You should develop an API where users can create and manage orders.
Items can be ordered and orders are automatically fulfilled as soon as the item stock allows it.

Specification

The system should be able to provide the following features:

- create, read, update and delete and list all entities;
- when an order is created, it should try to satisfy it with the current stock.;
- when a stock movement is created, the system should try to attribute it to an order that isn't complete;
- when an order is complete, send a notification by email to the user that created it;
- trace the list of stock movements that were used to complete the order, and vice-versa;
- show current completion of each order;
- Write a log file with: orders completed, stock movements, email sent and errors.

     */
}
