package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities;

import javax.persistence.*;

public class StockMovement {

    @Id
    @Column
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    @Column
    private final long creationDate;

    @MapsId
    @OneToMany
    @JoinColumn(name = "item_id")
    private Item item;

    @Column
    private int quantity;

    private StockMovement(Item item, int quantity, long creationDate) {
        this.item = item;
        this.quantity = quantity;
        this.creationDate = creationDate;
    }

    public static StockMovement from(Item item, int quantity, long creationDate) {
        return new StockMovement(item, quantity, creationDate);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}

