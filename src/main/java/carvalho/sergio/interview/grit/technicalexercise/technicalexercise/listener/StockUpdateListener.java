package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.listener;

import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.Central;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class StockUpdateListener implements ApplicationListener<StockUpdateEvent> {

    @Autowired
    Central central;


    @Override
    public void onApplicationEvent(StockUpdateEvent event) {
        central.attemptFulfillOrders(event.getItemType());
    }


}
