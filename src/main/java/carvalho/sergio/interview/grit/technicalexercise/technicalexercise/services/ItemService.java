package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.services;

import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.Item;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.repository.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class ItemService {

    @Autowired
    ItemsRepository itemsRepository;


    public List<Item> getAllItems() {
        List<Item> items = new ArrayList<>();
        itemsRepository.findAll().forEach(items::add);
        return items;
    }

    public Item save(String name) {

        if (getByName(name) == null) {
            return itemsRepository.save(Item.from(name));
        }

        throw new IllegalArgumentException(name + " already exists in the DB");

    }

    public Item update(String oldName, String newName) {
        Item item = getByName(oldName);
        item.setName(newName);
        return itemsRepository.save(item);

    }

    public Item getItemsById(int id) {
        return itemsRepository.findById(id).get();
    }

    public void delete(int id) {
        itemsRepository.deleteById(id);
    }

    public Item getByName(String name) {
        for (Item item : getAllItems()) {
            if (item.getName().equals(name)) {
                return item;
            }
        }

        throw new NoSuchElementException();
    }

}
