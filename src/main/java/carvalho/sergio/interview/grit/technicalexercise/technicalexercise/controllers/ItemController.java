package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.controllers;

import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.Item;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("item")
public class ItemController {
    @Autowired
    ItemService itemService;

    @GetMapping("/list")
    private List<Item> getAllItems() {
        return itemService.getAllItems();
    }

    @GetMapping({"/create/{name}"})
    private Item createItem(@PathVariable String name) {

       return itemService.save(name);

    }


    @GetMapping({"/update/{oldName}/{newName}"})
    private Item updateItem(@PathVariable String oldName, @PathVariable String newName) {

        return itemService.update(oldName, newName);

    }

    @GetMapping("/read/{id}")
    private Item getItem(@PathVariable("id") int id) {
        return itemService.getItemsById(id);
    }

    @GetMapping("/delete/{id}")
    private void deleteItem(@PathVariable("id") int id) {
        itemService.delete(id);
    }

}
