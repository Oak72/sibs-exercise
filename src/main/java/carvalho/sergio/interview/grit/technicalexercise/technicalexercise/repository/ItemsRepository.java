package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.repository;


import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.Item;
import org.springframework.data.repository.CrudRepository;

public interface ItemsRepository extends CrudRepository<Item, Integer> {

}
