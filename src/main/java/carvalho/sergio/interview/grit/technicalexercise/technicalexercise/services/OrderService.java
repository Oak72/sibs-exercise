package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.services;

import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.Order;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.User;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.repository.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    OrdersRepository ordersRepository;
    @Autowired
    ItemService itemService;
    @Autowired
    UserService userService;


    public List<Order> getAllOrders() {
        List<Order> orders = new ArrayList<>();
        ordersRepository.findAll().forEach(orders::add);
        return orders;
    }


    public Order createOrder(String userId, String itemType, String quantity) {
        return ordersRepository.save(
                Order.from(
                        itemService.getByName(itemType),
                        Integer.parseInt(quantity),
                        userService.getUsersById(Integer.parseInt(userId)),
                        System.currentTimeMillis()
                ));

    }

    public Order updateOrder(String id, String userId, String itemType, String quantity) {
        Order order = ordersRepository.findById(Integer.valueOf(id)).get();
        order.setUser(userService.getUsersById(Integer.parseInt(userId)));
        order.setItem(itemService.getByName(itemType));
        order.setQuantity(Integer.parseInt(quantity));
        return ordersRepository.save(order);
    }

    public Order getOrderById(int id) {
        return ordersRepository.findById(id).get();
    }

    public void delete(int id) {
        ordersRepository.deleteById(id);
    }

    public void delete(Order order) {
        ordersRepository.delete(order);
    }
}
