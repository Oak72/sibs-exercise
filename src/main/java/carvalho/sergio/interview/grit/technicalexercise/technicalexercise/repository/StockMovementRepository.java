package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.repository;

import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.StockMovement;
import org.springframework.data.repository.CrudRepository;

public interface StockMovementRepository  extends CrudRepository<StockMovement, Integer> {

}
