package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.controllers;

import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.Order;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.User;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("order")
public class OrderController {
    @Autowired
    OrderService orderService;

    @GetMapping("/list")
    private List<Order> getAllOrders() {
        return orderService.getAllOrders();
    }

    @GetMapping({"/create/{userId}/{itemType}/{quantity}"})
    private Order createOrder(
            @PathVariable String userId,
            @PathVariable String itemType,
            @PathVariable String quantity
    ) {

        return orderService.createOrder(
                userId,
                itemType,
                quantity
        );

    }

    @GetMapping({"/update/{orderId}/{userId}/{itemType}/{quantity}"})
    private Order updateOrder(
            @PathVariable String orderId,
            @PathVariable String userId,
            @PathVariable String itemType,
            @PathVariable String quantity
    ) {
        return orderService.updateOrder(
                orderId,
                userId,
                itemType,
                quantity
        );

    }

    @GetMapping("/read/{id}")
    private Order getOrder(@PathVariable("id") int id) {
        return orderService.getOrderById(id);
    }

    @GetMapping("/delete/{id}")
    private void deleteUser(@PathVariable("id") int id) {
        orderService.delete(id);
    }






}
