package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.listener;

import org.springframework.context.ApplicationEvent;

public class StockUpdateEvent extends ApplicationEvent {

    private String itemType;

    public StockUpdateEvent(Object source, String itemType) {
        super(source);
        this.itemType = itemType;
    }

    public String getItemType() {
        return itemType;
    }
}
