package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.repository;


import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface UsersRepository extends CrudRepository<User, Integer> {

}
