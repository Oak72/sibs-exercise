package carvalho.sergio.interview.grit.technicalexercise.technicalexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechnicalExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechnicalExerciseApplication.class, args);
	}

}
