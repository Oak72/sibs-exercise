package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.services;

import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.User;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    UsersRepository usersRepository;


    public List<User> getAllUsers() {
        List<User> users = new ArrayList<User>();
        usersRepository.findAll().forEach(u -> users.add(u));
        return users;
    }

    public User saveOrUpdate(String name, String email) {

        //Ideally I would just query the DB directly to get the user rather than doing it this way
        for (User user : getAllUsers()) {
            if (user.getEmail().equals(email)) {
                user.setName(name);
                user.setEmail(email);
                return usersRepository.save(user);
            }
        }

        return usersRepository.save(User.from(name,email));

    }

    public User getUsersById(int id) {
        return usersRepository.findById(id).get();
    }

    public void delete(int id)
    {
        usersRepository.deleteById(id);
    }

}
