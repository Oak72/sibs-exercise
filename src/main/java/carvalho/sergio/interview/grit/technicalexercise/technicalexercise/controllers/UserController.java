package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.controllers;

import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.User;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/list")
    private List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping({
            "/create/{name}/{email}",
            "/update/{name}/{email}"
    })
    private User createOrUpdateUser(@PathVariable String name, @PathVariable String email) {

        return userService.saveOrUpdate(name, email);

    }

    @GetMapping("/read/{id}")
    private User getUser(@PathVariable("id") int id) {
        return userService.getUsersById(id);
    }

    @GetMapping("/delete/{id}")
    private void deleteUser(@PathVariable("id") int id) {
        userService.delete(id);
    }

}
