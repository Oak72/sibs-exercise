package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.repository;


import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrdersRepository extends CrudRepository<Order, Integer> {

}
