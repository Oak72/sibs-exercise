package carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity
@Table(name = "users")
public class User {

    @Id
    @Column
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column(unique = true)
    private String email;


    private User(String name, String email) {
        this.name = name;
        this.email = email;
    }
    public static User from(String name, String email) {
        return new User(name,email);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
