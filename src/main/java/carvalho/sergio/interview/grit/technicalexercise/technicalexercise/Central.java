package carvalho.sergio.interview.grit.technicalexercise.technicalexercise;


import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.Order;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.model.entities.StockMovement;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.services.ItemService;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.services.OrderService;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.services.StockMovementService;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.services.UserService;
import carvalho.sergio.interview.grit.technicalexercise.technicalexercise.utility.EmailPublisher;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.Optional;

public class Central {

    @Autowired
    Logger logger;
    @Autowired
    ItemService itemService;
    @Autowired
    OrderService orderService;
    @Autowired
    StockMovementService stockMovementService;
    @Autowired
    UserService userService;


    @Autowired
    EmailPublisher emailPublisher;



    public Optional<Order> checkStock(String itemType) {

        final int total = stockMovementService.
                getAllStockMovements()
                .stream()
                .filter(
                stockMovement -> stockMovement.getItem().getName().equals(itemType)
        ).mapToInt(
                StockMovement::getQuantity
                )
                .sum();

        return orderService.getAllOrders().stream().filter(o -> o.getQuantity() <= total).findFirst();

    }

    public void fulfillOrder(Order order) {
        stockMovementService.save(order.getItem().getName(), "-" + order.getQuantity());
        orderService.delete(order);
        emailPublisher.notifyUser(order);

    }

    public void attemptFulfillOrders(String itemType) {

        try {
            if (checkStock(itemType).isPresent()) {
                fulfillOrder(checkStock(itemType).get());
            }
        } catch (Exception e) {

        }
    }

}
