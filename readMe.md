

For the sake of simplicity this program supports only GET requests, returning a BAD_REQUEST in case of any other request, or malformed requests.

The API works as follows:

/item/list

/item/create/{name}

/item/update/{oldName}/{newName}

/item/read/{id}

/item/delete/{id}

/order/list

/order/create/{userId}/{itemType}/{quantity}

/order//update/{orderId}/{userId}/{itemType}/{quantity}

/order/read/{id}

/order/delete/{id}

/stock/list

/stock/create/{itemType}/{quantity}

/stock/update/{stockMovementId}/{itemType}/{quantity}

/stock/read/{id}

/stock/delete/{id}

/user/list

/user/create/{name}/{email}

/user/update/{name}/{email}

/user/read/{id}

/user/delete/{id}

Note that Bad Requests can be due to, for example, trying to edit a user that does not exist or adding a stock for an itemType that doesn't exist yet.


-----

Run in a container with Postgres installed compatible with the following settings:

port:5432

database name: database

username=postgres


-----

Due to time constraints I wasn't able to fully introduce features that I believe would be vast improvements. For example, having an actual stock rather than the 'hack-ish' way to calculate current stock by querying the stock movements (which could in theory be altered), among with other QoL and good practices